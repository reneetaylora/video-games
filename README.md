Today people are arguing about whether people should pay video games. New international research has shown that playing action video games has a beneficial effect on the brain because of increased cognitive abilities such as perception, attention, and reaction time. The team, supervised by the University of Geneva , Switzerland, has conducted two meta-analysis over the last 15 years to see how video games affect cognition. Because the human brain is adaptable, learnable and adaptable, there are many previous studies that analyzed whether changes in the brain lead to improvements in perception, attention, and reaction time when playing action video games.

Now in the 21st century, video games are not just a hobby, they are part of a larger image. Some people even think of it as a platform for game execution. Thanks to the great appreciation for video games, it has strangely produced a series of discussions about whether it is good or bad for people. Well, video games are a mixture of both. And sometimes it's all in control. These are some of the advantages and disadvantages of playing video games.

Strengthen relationships

Video games are an excellent hobby for families. Strengthen bonds and build closer relationships with your family. This makes it easy for parents to interact with their children. In addition, links are limited to a variety of information technologies, not just one area.

Improve problem solving skills

These virtual cognitive games are not only fun but also beneficial to the brain. Almost all video games have specific rules to follow. That means you need to think carefully before moving. As time goes on, players will also learn to make decisions in an instant. The creativity and consistency of video games improves the player's cognitive status. This is because most games present difficult challenges and difficult stages to complete. These aspects help to improve the problem-solving skills of children and adults.

Help dyslexic children read better

A study by the University of Padua sheds cold water on the idea that video games are bad for an infant's brain. In February, Italian researchers provided evidence that playing fast-paced video games can improve the reading comprehension of children with dyslexia. The team divided children aged 7 to 13 into two groups, one played an action game called “Rayman Raving Rabids” and the other played a low-tempo game. When children's reading skills were later tested, people who played action games were able to read faster and more accurately. The authors of this study hypothesized that action games help children reach attention.

To sum up

Video games are a good way of fun and entertainment. Help the player to be himself and forget the real world for a while. However, to avoid reliance on video games, you must consider the importance of controls. This is not only harmful, but also fatal. Learn to balance everything and set the play time for this type of activity. If you want to know what games are popular and the latest video game news , you can visit https://www.gamemite.com/news/.